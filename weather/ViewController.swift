//
//  ViewController.swift
//  weather
//
//  Created by imac on 23/02/2018.
//  Copyright © 2018 Ketmen. All rights reserved.
//

import UIKit



class ViewController: UIViewController , cityTrans {
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var dateLable: UILabel!
    
    @IBOutlet weak var cityLable: UILabel!
    @IBOutlet weak var timpLable: UILabel!
    @IBOutlet weak var sunnyLable: UILabel!
    var cityName = "泽普"
    override func viewDidLoad() {
        super.viewDidLoad()
        menuBtn.layer.shadowColor = UIColor.gray.cgColor
        menuBtn.layer.shadowOffset = CGSize(width: 6, height: 6)
        menuBtn.layer.shadowOpacity = 1
        menuBtn.layer.shadowRadius = 6
        loadJSon(city: "101130101")
    }
    
    func trans(item: Any) {
        let item = item as! (key:String,value:String)
        cityName = item.key
        loadJSon(city: item.value)
    }
    func loadJSon(city:String)  {
        //https://www.sojson.com/open/api/weather/json.shtml?city=XXX
        //http://wthrcdn.etouch.cn/weather_mini?citykey=101010100
        GETJson("http://wthrcdn.etouch.cn/weather_mini", ["citykey":city]) { (json, error) in
            if (error != nil ) {
                print(" JSON ----- error")
                return
            }
            if json!["status"] as? Int ?? 0 == 1000 {
                do {
                    let data = try JSONSerialization.data(withJSONObject:  json!, options: .prettyPrinted)
                    let coda = JSONDecoder()
                    let res = try? coda.decode(HomeModel.self, from: data)
                    self.homeData = res;
                } catch{
                    print("error");
                }
            }
        }
    }
    var homeData : HomeModel? = nil {
        didSet{
            tableview.reloadData()
            imageView.image = UIImage(named: sunnyImage(str: homeData?.data?.forecast![0].type ?? ""))
            self.sunnyLable.text = sunny(str: homeData?.data?.forecast![0].type ?? "")
            self.timpLable.text = homeData?.data?.wendu ?? ""
            self.cityLable.text = cityName
            let dateFormet = DateFormatter()
            dateFormet.dateFormat = "YYYY/MM/dd"
            dateLable.text = todayZHasUG(str: homeData?.data?.forecast![0].date ?? "") + " " +  dateFormet.string(from: Date())
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CityTableViewController" {
            let vc = segue.destination as! CityTableViewController
            vc.delegate = self
        }
    }
}
extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (homeData?.data?.forecast?.count ?? 0)-1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! WheatherTableViewCell
        cell.itemData = homeData?.data?.forecast![indexPath.row+1]
        cell.bacimageView.alpha = 0.5
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80;
    }
}

//星期一： Mon.=Monday
//星期二： Tues.=Tuesday
//星期三：Wed.=Wednesday
//星期四： Thur.=Thursday
//星期五： Fri.=Friday
//星期六： Sat.=Saturday
//星期天： Sun.=Sunday

func todayZHasUG(str:String)->String {
    var res = ""
    for i in str {
        let s:[Character] = ["日","星","期","一","二","三","四","五","六","天"]
        for j in s {
            if i == j {
                res.append(i)
            }
        }
    }
    var todayZH  = ""
    switch res {
    case "日星期一": todayZH = "دۈشەنبە"; break
    case "日星期二": todayZH = "سەيشەنبە"; break
    case "日星期三": todayZH = "چارشەنبە"; break
    case "日星期四": todayZH = "پەيشەنبە"; break
    case "日星期五": todayZH = "جۈمە"; break
    case "日星期六": todayZH = "شەنبە"; break
    case "日星期天": todayZH = "يەكشەنبە"; break
    default:
        break
    }
    return todayZH
}

func sunnyImage(str:String) -> String {
    switch str {
    case "多云":
        return "tttt"
    case "晴":
        return "sunny"
    case "阴":
        return "smog"
    case "小雨":
        return "rain"
    case "阵雨":
        return "snowy"
    case "小雪":
        return "rain"
    case "阵雪":
        return "snowy"
    default:
        return "sunny"
    }
}
func sunny(str:String) -> String {
    switch str {
    case "多云":
        return "بۇلۇتلۇق"
    case "晴":
        return "ھاۋا ئوچۇق"
    case "阴" :
        return "ھاۋا تۇتۇق"
    case "小雨":
        return "ئازراق يامغۇر ياغىدۇ"
    case "阵雨":
        return "ئۆتكۈنچە يامغۇر"
    case "小雪":
        return "ئازراق قار ياغىدۇ"
    case "阵雪":
        return "ئۆتمۈنچە قار"
    default:
        return "نامەلۇم"
    }
}

// --------Model--------
struct HomeModel : Codable {
    var date   : String?
    var status : Int?
    var data   :ContentData?
    
    enum CodingKeys :String,CodingKey {
        case date     = "date"
        case status   = "status"
        case data     = "data"
    }
}
struct ContentData : Codable {
    var city      : String?
    var wendu     : String?
    var ganmao    : String?
    var yesterday : DayModel?
    var forecast  :[DayModel]?
    
    enum CodingKeys :String,CodingKey {
        case yesterday = "yesterday"
        case city      = "city"
        case wendu     = "wendu"
        case ganmao    = "ganmao"
        case forecast  = "forecast"
    }
    
}
struct DayModel:Codable {
    var date    : String?
    var sunrise : String?
    var sunset  : String?
    var high    : String?
    var low     :String?
    var aqi     : Int?
    var fx      : String?
    var fl      : String?
    var type    : String?
    var notice :String?
    enum CodingKeys :String,CodingKey {
        case date    = "date"
        case sunrise = "sunrise"
        case sunset  = "sunset"
        case high    = "high"
        case low     = "low"
        case aqi     = "aqi"
        case fx      = "fx"
        case fl      = "fl"
        case type    = "type"
        case notice  = "notice"
        
    }
}
