//
//  JsonAs.h
//  JsonModel
//
//  Created by mac on 2017/1/4.
//  Copyright © 2017年 Arsilan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonAs : NSObject

+(void) JsonAnalysisBody:(NSString *)body Mode:(NSString *)mode completion:(void(^)(NSDictionary *JsonDict))completion errorCompletion:(void(^)(NSError * error))errorCompletion;

@end

id GETJson(NSString* path, NSDictionary* param, void(^completion)(NSDictionary* json, NSError* error));

void POSTJson(NSString* path, NSDictionary* param, NSDictionary* bodyParam, void(^completion)(NSDictionary* json, NSError* error));



/** 为机票搜索页添加 by Murat */
id GETURL(NSString* path, NSDictionary* param);

id GETJsonAndTask(NSString* path, NSDictionary* param, void(^completion)(NSDictionary* json, NSError* error));


NSString* currentSessionId();

void setCurrentSesssionId(NSString* sessionId);
