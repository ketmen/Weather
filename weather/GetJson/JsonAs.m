//
//  JsonAs.m
//  JsonModel
//
//  Created by mac on 2017/1/4.
//  Copyright © 2017年 Arsilan. All rights reserved.
//

#import "JsonAs.h"

NSString * BaseURL = @"http://10.2.10.38/index.php";


static NSString* NSStringFromQueryParameters(NSDictionary* queryParameters);

static NSURL* NSURLByAppendingQueryParameters(NSURL* URL, NSDictionary* queryParameters);

static NSURLCache* JSONResponseCache();

@implementation JsonAs

+(void) JsonAnalysisBody:(NSString *)body Mode:(NSString *)mode completion:(void(^)(NSDictionary *JsonDict))completion errorCompletion:(void(^)(NSError * error))errorCompletion
{
    
    if ([mode isEqualToString:@"post"]) {
        
        POSTJson(body, nil, nil, ^(NSDictionary *json, NSError *error) {
            if (json) {
                completion(json);
            }
            else {
                errorCompletion(error);
            }
        });
        
    }else{
        
            GETJson(body, nil, ^(NSDictionary *json, NSError *error) {
                if (json) {
                    completion(json);
                }
                else {
                    errorCompletion(error);
                }
            });
    }
}

@end

id GETJson(NSString* path, NSDictionary* param, void(^completion)(NSDictionary* json, NSError* error))
{
    
    if (path == nil) {
        path = @"";
    }
    
    NSMutableDictionary* internalParam = [param mutableCopy];
    [internalParam addEntriesFromDictionary:@{@"r_type": @"1"}];
    
    NSURL* url = nil;
    
    if ([path hasPrefix:@"http"] || [path hasPrefix:@"https"]) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",path]];
        url = NSURLByAppendingQueryParameters(url, internalParam);
    }
    else {
        if (path == nil || path.length ==  0 ) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",BaseURL]];
        }
        else {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",BaseURL,path]];
        }
        url = NSURLByAppendingQueryParameters(url, internalParam);
    }

    
    NSURLSession * session = [NSURLSession sharedSession];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    NSLog(@"%@",url);
    
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data != nil) {

            NSCachedURLResponse* cached = [[NSCachedURLResponse alloc] initWithResponse:response data:data];
            [JSONResponseCache() storeCachedResponse:cached forRequest:request];
            
            NSError* jsonError = nil;
            id dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&jsonError];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (dict) {
                    completion(dict, nil);
                }
                else {
                    completion(nil, jsonError);
                    NSLog(@"%@", jsonError);
                }
            });
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, error);
                NSLog(@"%@",error);
            });
        }
    }];
    
    [dataTask resume];
    
    NSData* data = [[JSONResponseCache() cachedResponseForRequest:request] data];
    if (data != nil) {
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        return json;
    }
    return nil;
    
}

 void POSTJson(NSString* path, NSDictionary* param, NSDictionary* bodyParam, void(^completion)(NSDictionary* json, NSError* error))
{
    NSLog(@"%@",bodyParam);
    NSMutableDictionary* internalParam = [param mutableCopy];
//    [internalParam addEntriesFromDictionary:@{@"r_type": @"1"}];
    
    NSURL* url = nil;
    
    if ([path hasPrefix:@"http"] || [path hasPrefix:@"https"]) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",path]];
        url = NSURLByAppendingQueryParameters(url, internalParam);
    }
    else {
        
        if (path == nil || path.length ==  0 ) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",BaseURL]];
        }
        else {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",BaseURL,path]];
        }
        url = NSURLByAppendingQueryParameters(url, internalParam);
    }

    

    NSMutableURLRequest * requset = [NSMutableURLRequest requestWithURL:url];
    requset.HTTPMethod = @"POST";
    NSString* body = NSStringFromQueryParameters(bodyParam);
    requset.HTTPBody = [body dataUsingEncoding:NSUTF8StringEncoding];
   
    NSLog(@"Post:>>: %@",requset);
    
    NSURLSession * session = [NSURLSession sharedSession];

    NSURLSessionDataTask *datatack = [session dataTaskWithRequest:requset completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data != nil) {
            NSError* jsonError = nil;
            id dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&jsonError];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (dict) {
                    completion(dict, nil);
                    //NSLog(@"%@",dict);
                }
                else {
                    completion(nil, jsonError);
                    NSLog(@"%@", jsonError);
                }
            });
            
        } else{
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, error);
                NSLog(@"%@",error);
            });
        }
    }];
    
    
    [datatack resume];
}

/** 为机票搜索页添加 by Murat */
id GETURL(NSString* path, NSDictionary* param){
    if (path == nil) {
        path = @"";
    }
    
    NSMutableDictionary* internalParam = [param mutableCopy];
    [internalParam addEntriesFromDictionary:@{@"r_type": @"1"}];
    
    NSURL* url = nil;
    
    if ([path hasPrefix:@"http"]) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",path]];
        url = NSURLByAppendingQueryParameters(url, internalParam);
    }
    else {
        if (path == nil || path.length ==  0 ) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",BaseURL]];
        }
        else {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",BaseURL,path]];
        }
        url = NSURLByAppendingQueryParameters(url, internalParam);
    }

    return url;
}

id GETJsonAndTask(NSString* path, NSDictionary* param, void(^completion)(NSDictionary* json, NSError* error)){
    
    if (path == nil) {
        path = @"";
    }
    
    NSMutableDictionary* internalParam = [param mutableCopy];
    [internalParam addEntriesFromDictionary:@{@"r_type": @"1"}];
    
    NSURL* url = nil;
    
    if ([path hasPrefix:@"http"]) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",path]];
        url = NSURLByAppendingQueryParameters(url, internalParam);
    }
    else {
        if (path == nil || path.length ==  0 ) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",BaseURL]];
        }
        else {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",BaseURL,path]];
        }
        url = NSURLByAppendingQueryParameters(url, internalParam);
    }
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    NSLog(@"Get:>: %@",url);
    
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data != nil) {
            
            NSError* jsonError = nil;
            id dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&jsonError];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (dict) {
                    completion(dict, nil);
                    //NSLog(@"%@",dict);
                }
                else {
                    completion(nil, jsonError);
                    NSLog(@"%@", jsonError);
                }
            });
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, error);
                NSLog(@"%@",error);
            });
        }
    }];
    
    [dataTask resume];
    
    return dataTask;
}


static NSURLCache* JSONCache = nil;

static NSURLCache* JSONResponseCache() {
    
    if (JSONCache == nil) {
        JSONCache = [[NSURLCache alloc] initWithMemoryCapacity:50 * 1024 * 1024 diskCapacity:50 * 1024 * 1024 diskPath:@"json_cache"];
    }
    return JSONCache;
}




/*
 * Utils: Add this section before your class implementation
 */

/**
 This creates a new query parameters string from the given NSDictionary. For
 example, if the input is @{@"day":@"Tuesday", @"month":@"January"}, the output
 string will be @"day=Tuesday&month=January".
 @param queryParameters The input dictionary.
 @return The created parameters string.
 */
static NSString* NSStringFromQueryParameters(NSDictionary* queryParameters)
{
    NSMutableArray* parts = [NSMutableArray array];
    [queryParameters enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
        NSString *part = [NSString stringWithFormat: @"%@=%@",
                          key,
                          value
                          ];
        [parts addObject:[part stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    }];
    return [parts componentsJoinedByString: @"&"];
}

/**
 Creates a new URL by adding the given query parameters.
 @param URL The input URL.
 @param queryParameters The query parameter dictionary to add.
 @return A new NSURL.
 */
static NSURL* NSURLByAppendingQueryParameters(NSURL* URL, NSDictionary* queryParameters)
{
    NSString* URLString = [NSString stringWithFormat:@"%@?%@",
                           [URL absoluteString],
                           NSStringFromQueryParameters(queryParameters)
                           ];
    return [NSURL URLWithString:URLString];
}


static NSString* sessionID = @"084e3ef7417b84868123b1b7e4764388";

NSString* currentSessionId() {
    return sessionID;
}

void setCurrentSesssionId(NSString* sessionId) {
    sessionID = sessionId;
}
