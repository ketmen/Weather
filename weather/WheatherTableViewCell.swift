//
//  WheatherTableViewCell.swift
//  weather
//
//  Created by imac on 23/02/2018.
//  Copyright © 2018 Ketmen. All rights reserved.
//

import UIKit

protocol cityTrans {
    func trans(item:Any)
}

class WheatherTableViewCell: UITableViewCell {

    @IBOutlet weak var bacimageView : UIImageView!
    @IBOutlet weak var dateLable    : UILabel!
    @IBOutlet weak var timpLale     : UILabel!
    @IBOutlet weak var weekLable    : UILabel!
    @IBOutlet weak var lowtimpLable : UILabel!
    @IBOutlet weak var sunnyLable   : UILabel!
    var itemData: DayModel? = nil {
        didSet{
            bacimageView.image = UIImage(named: sunnyImage(str: itemData?.type ?? ""))
            sunnyLable.text    = sunny(str: itemData?.type ?? "")
            dateLable.text     = resDate(str: itemData?.date ?? "")
            weekLable.text     = todayZHasUG(str: itemData?.date ?? "01/01") == "" ? "نامەلۇم" : todayZHasUG(str: itemData?.date ?? "01/01")
            timpLale.text      = resStr(str: (itemData?.high) ?? "")
            lowtimpLable.text  = resStr(str: itemData?.low ?? "")
        }
    }
     

    func myDate (str:String) -> Date {
        let d = DateFormatter()
        d.dateFormat = "yyyy"
        d.timeZone = NSTimeZone.system
        let year = d.string(from: Date())
        d.dateFormat = "yy/mm/dd"
        let r = d.date(from: year+"/"+str)
        return r != nil ? r! : Date()
    }
    
    func resDate(str:String) -> String {
        var res = ""
        for i in str {
            let s:[Character] = ["0","1","2","3","4","5","6","7","8","9"]
            for j in s {
                if i == j {
                    res.append(i)
                }
            }
        }
        let a = DateFormatter()
            a.dateFormat = "MM/"
        let d = DateFormatter()
            d.dateFormat = "dd"
        let tuday = d.string(from: Date())
        if (Int(res) ?? 0 >= getDaysInCurrentMonth()) && Int(tuday) ?? 0 != getDaysInCurrentMonth(){
            return "\(Int(a.string(from: Date())) ?? 0 == 12 ? 1 : Int(a.string(from: Date())) ?? 0 + 1)"+res
        }
        return a.string(from: Date()) + res
    }
    
    func getDaysInCurrentMonth() -> Int {
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: .gregorian)!
        var comps: NSDateComponents = NSDateComponents()
        comps = calendar.components([.year,.month,.day, .weekday, .hour, .minute,.second], from: Date()) as NSDateComponents
        let year =  comps.year
        let month = comps.month
        let startComps = NSDateComponents()
        startComps.day = 1
        startComps.month = month
        startComps.year = year
        let endComps = NSDateComponents()
        endComps.day = 1
        endComps.month = month == 12 ? 1 : month + 1
        endComps.year = month == 12 ? year + 1 : year
        let startDate = calendar.date(from:startComps as DateComponents)!
        let endDate = calendar.date(from:endComps as DateComponents)!
        let diff = calendar.components(.day, from: startDate, to: endDate,options: .matchFirst)
        return diff.day ?? 0
    }
    
    func resStr(str:String) -> String {
        var res = ""
        for (index,i) in str.enumerated() {
            if index > 1 {
                if (index > 1 && index != str.count) {
                    res.append(i)
                }
            }
        }
        return res
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   

}
